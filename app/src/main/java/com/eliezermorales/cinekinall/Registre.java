package com.eliezermorales.cinekinall;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class Registre extends ActionBarActivity {

    EditText txtUsuarioRegistrar,txtPassRegistrar,txtPassVerificar, txtNyA, txtCorreo;
    Button btnRegistrarUsuario;
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registre);
        rl = (RelativeLayout)findViewById(R.id.registrar);
        btnRegistrarUsuario= (Button) findViewById(R.id.btnRegistrarUsuario);
        txtUsuarioRegistrar = (EditText) findViewById(R.id.txtUsuarioRegistrar);
        txtPassRegistrar = (EditText) findViewById(R.id.txtPassRegistrar);
        txtPassVerificar = (EditText) findViewById(R.id.txtVerificarPass);
        txtNyA = (EditText) findViewById(R.id.txtNyA);
        txtCorreo = (EditText) findViewById(R.id.txtCorreo);
        Bundle miBundle = this.getIntent().getExtras();

        try{
            btnRegistrarUsuario.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (txtUsuarioRegistrar.getText().toString().matches("") || txtPassRegistrar.getText().toString().matches("") || txtPassVerificar.getText().toString().matches("") || txtNyA.getText().toString().matches("") || txtCorreo.getText().toString().matches("")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Datos Erroneos", Toast.LENGTH_LONG);
                        toast.show();



                    } else if (txtPassRegistrar.getText().toString().matches(txtPassVerificar.getText().toString())) {
                        UserManager.getInstancia().agregarUsuario(txtUsuarioRegistrar.getText().toString(),txtPassRegistrar.getText().toString(), txtNyA.getText().toString(), txtCorreo.getText().toString());

                        Toast t = Toast.makeText(getApplicationContext(),txtUsuarioRegistrar.getText().toString()+" Registrado",Toast.LENGTH_SHORT);
                        Intent myIntent = new Intent(view.getContext(), Login.class);

                        Bundle b = new Bundle();
                        b.putString("Usuario",txtUsuarioRegistrar.getText().toString());



                        startActivityForResult(myIntent, 0);

                        t.show();
                        myIntent.putExtras(b);
                        startActivity(myIntent);


                    } else if (txtPassRegistrar.getText().toString() != txtPassVerificar.getText().toString()) {
                        Toast t = Toast.makeText(getApplicationContext(), "Contraseña erronea, no es igual a contraseña anterior", Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            });
        }catch (NullPointerException e){

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registre, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

